public class Euler17{
	
	public static void main(String[] args){
		String[] units = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
		String[] tens = {"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
		String[] tens2 = {"twenty", "thirty", "forty", "fifty", "sixy", "seventy", "eighty", "ninety"};
		String hundred = "hundred";
		String thousand = "thousand";
		String and = "and";
		int count = 0;

		// Count letters in 1-9
		for(int i = 0; i < units.length; i++){
			count += units[i].length();
		}

		// Count letters in 10-19
		for(int i = 0; i < tens.length; i++){
			count += tens[i].length();
		}

		// Count letters in 20...90
		for(int i = 0; i < tens2.length; i++){
			count += tens2[i].length();
		}

		// Count letters in 21..99 (excluding 20..90)
		for(int i = 0; i < tens2.length; i++){
			for(int j = 0; j < units.length; j++) {
			count += tens2[i].length();
			count += units[j].length();
			}
		}

		for()

		// Print answer
		System.out.println("The number of letters is: "+count);
	}
}