public class Euler5{
	public static void main(String[] args){
		boolean possibleNum = true;

		for(int i = 20; true; i+=2){
			possibleNum = true;
			for(int j = 2; j < 21 && possibleNum; j++){
				if(i % j != 0){
					possibleNum = false;
				}
			}
			if(possibleNum){
				System.out.println(i);
				System.exit(0);
			}
		}
	}
}
