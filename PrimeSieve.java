import java.util.*;
import java.math.*;

public class PrimeSieve{

	public static ArrayList<Integer> primeList(int n){
		n++;
		boolean[] isPrime = new boolean[n];
		
		// Set all numbers to be primes
		for(int i = 2; i < n; i++){
			isPrime[i] = true;
		}
		
		int j = 0;
		for(int i = 1; i <= n/2; i++){
			if(isPrime[i]){
				for(j = 2*i; j < n; j=j+i){
					isPrime[j] = false;
				}
			}
		}
		
		// Add all primes to an ArrayList of type Integer
		ArrayList<Integer> primes = new ArrayList<Integer>();
		for(int i = 2; i < isPrime.length; i++){
			if(isPrime[i]) primes.add(new Integer(i));
		}
		
		return primes;
	}
	
	public static void main(String[] args){
		/*ArrayList<Integer> list = primeList(2000000);
		
		/*for(int i = 0; i < list.size(); i ++){
			System.out.println(list.get(i));
		}*/
	
	}

}

