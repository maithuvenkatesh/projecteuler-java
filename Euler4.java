import java.util.*;

public class Euler4{
	
	public static boolean isPalindrome(int a){
		// Convert integer to string
		String toString = ""+a;
		String tempCopy = ""+a;
		String reverse = "";
		
		// Reverse String
		for(int i = tempCopy.length()-1; i >= 0; i--){
			reverse += tempCopy.charAt(i);
		}
		
		// Check if string is a palindrome
		if(reverse.equals(toString)) return true;
		return false;
	}
	
	public static void main(String[] args){
		int largestPalindrome = 0;
	
		for(int i = 999; i > 99; i--){
			for(int j = 998; j > 100 ; j--){
				if(isPalindrome(i*j) && (i*j) > largestPalindrome){
					largestPalindrome = i*j;
				}
			}
		}
		System.out.println(largestPalindrome);
	}
}
