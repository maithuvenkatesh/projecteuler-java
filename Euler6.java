public class Euler6{
	public static void main(String[] args){
		long squareOfSum = 0;
		for(int i = 0; i < 101; i++){
			squareOfSum += i*i;
		}
		//System.out.println(squareOfSum);
		
		long sumOfSquares = 0;
		for(int i = 0; i < 101; i++){
			sumOfSquares += i;
		}
		sumOfSquares = sumOfSquares*sumOfSquares;
		//System.out.println(sumOfSquares);
		
		System.out.println(sumOfSquares - squareOfSum);
		
	}
}
