import java.io.*;
import java.util.*;

public class Euler11{
	public static void main(String[] args) throws FileNotFoundException{
		FileReader fr = new FileReader("Euler11.txt");
		Scanner scan = new Scanner(fr);
		int[][] numberGrid = new int[20][20];

		//Read grid into 2D array
		while(scan.hasNextInt()){
			for(int r = 0; r < numberGrid.length; r++){
				for(int c = 0; c < numberGrid[r].length; c++){
					numberGrid[r][c] = scan.nextInt();
				}
			}
		}
		
		int greatestProduct = 0;
		int a,b,c,d;
		
		//Find greatest horizontal product
		for(int i = 0; i < numberGrid.length; i++){
			for(int j = 0; j < numberGrid[i].length-3; j++){
				a = numberGrid[i][j];
				b = numberGrid[i][j+1];
				c = numberGrid[i][j+2];
				d = numberGrid[i][j+3];
				int temp = a * b * c * d;
				if(temp > greatestProduct) greatestProduct = temp;
			}
		}	
			
		//Find greatest vertical product
		for(int y = 0; y < numberGrid.length; y++){
			for(int x = 0; x < numberGrid[y].length-3; x++){
				a = numberGrid[x][y];
				b = numberGrid[x+1][y];
				c = numberGrid[x+2][y];
				d = numberGrid[x+3][y];
				int temp = a * b * c * d;
				if(temp > greatestProduct) greatestProduct = temp;
			}
		}
		
		//Find greatest product diagonally (right to left)
		for(int w = 20; w > numberGrid.length-17; w--){
			for(int v = 20; v > numberGrid.length-17; v--){
				a = numberGrid[numberGrid.length-v][numberGrid.length-w];
				b = numberGrid[numberGrid.length-v+1][numberGrid.length-w+1];
				c = numberGrid[numberGrid.length-v+2][numberGrid.length-w+2];
				d = numberGrid[numberGrid.length-v+3][numberGrid.length-w+3];
				int temp = a * b * c * d;
				//if(w == 20 && v == 20 || w == 4 && v == 4) System.out.println(a + " " + b + " " + c + " " + d);
				if(temp > greatestProduct) greatestProduct = temp;
			}
		}
		System.out.println(greatestProduct);
		
		//Find greatest product diagonally (left to right)
		for(int m = 0; m < numberGrid.length-3; m++){
			for(int n = 20; n > numberGrid.length-17; n--){
				a = numberGrid[numberGrid.length-n][numberGrid.length-m-1];
				b = numberGrid[numberGrid.length-n+1][numberGrid.length-m-2];
				c = numberGrid[numberGrid.length-n+2][numberGrid.length-m-3];
				d = numberGrid[numberGrid.length-n+3][numberGrid.length-m-4];
				int temp = a * b * c * d;
				//if(m == 20 && n == 0 || m == 16 && n ==4) System.out.println(a + " " + b + " " + c + " " + d);
				if(temp > greatestProduct) greatestProduct = temp;
			}
		}
		System.out.println(greatestProduct);
	}
}
