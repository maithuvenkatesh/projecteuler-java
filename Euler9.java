public class Euler9{

	public static boolean isPythagoreanTriplet(int a,int b,int c){
		if(a*a + b*b == c*c) return true;
		else return false;
	}
	
	public static void main(String[] args){
		int product = 0;
		
		for(int a = 1; a < 1000; a++){
			for(int b = 2; b < 999; b++){
				for(int c = 3; c < 998; c++){
					if(isPythagoreanTriplet(a,b,c) && (a+b+c == 1000)) product = a*b*c;
				}
			}
		}
		System.out.print(product);
	}
}
