public class Euler21{
	
	public static boolean isAmicableNo(int a){
		int properDivisorsSumA = 0;
		
		for(int i = 1; i < a; i++){
			if(a % i == 0) properDivisorsSumA += i;
		}
		
		int b = properDivisorsSumA;
		int properDivisorsSumB = 0;
		
		for(int j = 1; j < b; j++){
			if(properDivisorsSumA % j == 0) properDivisorsSumB += j;
		}
		
		if(properDivisorsSumA == b && properDivisorsSumB == a && a!=b) return true;
		return false;	
	}
	
	public static void main(String[] args){
		int amicableSum = 0;
		
		for(int i = 1; i < 10001; i++){
			if(isAmicableNo(i)) amicableSum += i;
		}
		
		System.out.println(amicableSum);
	}
}
