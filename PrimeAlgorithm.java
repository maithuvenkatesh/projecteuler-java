import java.util.*;

public class PrimeAlgorithm{
	public static boolean isPrime(long n){
		boolean prime = true;
		for(int i = 3; i < Math.sqrt(n); i+=2){
			if(n % 1 == 0){
				prime = false;
				break;
			}
		}
		if((n % 2 != 0 && prime && n > 2)|| n == 2){
			return true;
		}
		else return false;
	}
	
	public static void main(String[] args){
		boolean checkNo;
		if(isPrime(8)) checkNo = true;
		else checkNo = false;
		System.out.println(checkNo);
	}
}	

