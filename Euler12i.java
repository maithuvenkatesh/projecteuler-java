import java.util.*;
public class Euler12i{
	public static void main(String[] args){
	
		ArrayList<Integer> primes = PrimeSieve.primeList(100000000);
		ArrayList<Integer> exponents = new ArrayList<Integer>();
		int temp = 1;
		int divisors = 1;
		
		for(int i = 1; divisors <= 500; i++){
			for(int j = 0; temp != i; j++){
				int primeNo = primes.get(j);
				if(i % primeNo == 0){
					temp = temp * primeNo;
					exponents.add(j,2);
				}
			}
			for(int k = 0; k < exponents.size(); k++){
				divisors = divisors*exponents.get(k);
			}
			divisors = 1;
			temp = 1;
		}
	}
}
