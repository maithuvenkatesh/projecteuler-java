import java.io.*;
import java.util.*;
import java.math.*;

public class Euler13{
	
	public static void main(String[] args) throws FileNotFoundException{
		FileReader fr = new FileReader("Euler13.txt");
		Scanner scan = new Scanner(fr);
		ArrayList<BigInteger> numbers = new ArrayList<BigInteger>();
		
		while(scan.hasNextBigInteger()){
				numbers.add(scan.nextBigInteger());
		}
		
		BigInteger sum = new BigInteger("0");
		for(int i = 0; i < numbers.size(); i++){
			sum = sum.add(numbers.get(i));
		}
		
		String sum10Digits = sum.toString();
		System.out.println(sum10Digits.substring(0,10));
	}
}
