public class Euler14{	
	public static void main(String[] args){
		long terms = 0L;
		long tempTerms = 0L;
		long startingNumber = 0L;
		
		for(long i = 2L; i <= 1000000; i++){
			long currentTerm = i;
			tempTerms++;
			while(currentTerm != 1){
				if(currentTerm % 2 == 0){
					currentTerm = currentTerm/2;
					tempTerms++;
				}
				else{
					currentTerm = (3*currentTerm)+1;
					tempTerms++;
				}
			}
					
			if(tempTerms > terms){
				terms = tempTerms;
				startingNumber = i;
			}
			tempTerms = 0L;
		}
		System.out.println(startingNumber);			
	}

}

