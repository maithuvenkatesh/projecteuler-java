/* To find all the prime numbers less than or equal to a given integer n by Eratosthenes' method:
   Create a list of consecutive integers from two to n: (2, 3, 4, ..., n).
   Initially, let p equal 2, the first prime number.
   Starting from p, count up by p and cross out thus found numbers in the list (which will be 2p, 3p, 4p, etc.).
   Find the first number not yet crossed out after p; let p now equal this number (which is the next prime).
   Repeat steps 3 and 4 until p is greater than n.
   All the numbers in the list which are not crossed out are prime. */

import java.util.*;

public class PrimeSieveFalse{	
	public static ArrayList<Integer> primeSieve(int n){
			ArrayList<Integer> primes = new ArrayList<Integer>();
			boolean isPrime;			
			int p = 0;
			
			for(int i = 2; i <= n; i++){
				p = i; 
				isPrime = false;
				if(p > n) break;
				else{
					for(int j = p; j <= n; j+=p){
						if(p % j != 0){
							 isPrime = true;
							 primes.add(p);
						 }
						else isPrime = false;
					}
				}
			}
			return primes;
	}
	
	public static void main(String[] args){
		System.out.print(PrimeSieve.primeSieve(10));

	}
}
