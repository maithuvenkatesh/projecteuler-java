import java.math.*;

public class Euler10{
	
	public static void main(String[] args){
		boolean[] primeList = PrimeSievePrimitive.primeList(2000000);
		BigInteger primeSum = new BigInteger("0");
		
		for(int i = 2; i < primeList.length; i++){
			if(primeList[i]) primeSum = primeSum.add(new BigInteger("" + i));
		}
		
		System.out.print(primeSum);
			
		
	}
}
