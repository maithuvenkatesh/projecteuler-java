import java.util.*;
import java.math.*;

public class PrimeSievePrimitive{

	public static boolean[] primeList(int n){
		n = n+1;
		boolean[] primes = new boolean[n];
		
		// Set all numbers to be primes
		for(int i = 2; i < primes.length; i++){
			primes[i] = true;
		}
		
		int j = 0;
		for(int i = 1; i <= n/2; i++){
			if(primes[i]){
				for(j = 2*i; j < n; j=j+i){
				primes[j] = false;
				}
			}
		}
		
		return primes;
	}
	
	public static void main(String[] args){
		boolean[] list = primeList(10);
		
		for(int i = 2; i < list.length; i ++){
			System.out.println(i + " " + list[i]);
		}
	
	}

}

