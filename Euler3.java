import java.math.*;
import java.util.*;

public class Euler3{
	
	public static boolean isPrime(long n){
		ArrayList<Boolean> primesBool = new ArrayList<Boolean>();
		
		for(long i = 2L; i < Math.sqrt(n); i++){
			if(n % i == 0) primesBool.add(new Boolean(true));
			else primesBool.add(new Boolean(false));
		}
		
		return(!(primesBool.contains(new Boolean(true))));
	}
	
	public static void main(String[] args){
		long compositeNo = 600851475143L;
		long primeFactor = 0L;
	
		for(long i = 2L; i < Math.sqrt(compositeNo); i++){
			if(isPrime(i) && (compositeNo % i == 0)){
				primeFactor = i;
				System.out.println(primeFactor);
			}	
		}
	}
			
}


