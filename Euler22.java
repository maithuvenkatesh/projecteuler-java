import java.io.*;
import java.util.*;

public class Euler22 {

	public int calculateScore(String name, int position) {
		int score = 0;
		for(int j = 0; j < name.length(); j++) {
			score += name.charAt(j);
		}
		// Convert ASCII value to alphabet position value
		score = score - (64*name.length());

		// Multiply score by position
		score = score * position;
		return score;
	}

	public static void main(String[] args) {
		List<String> names = new ArrayList<String>();
		File file = new File("names.txt");

		// Read names into ArrayList and replace all "" with \n
		try {
			Scanner scanner = new Scanner(file);
			scanner.useDelimiter(",");
			while(scanner.hasNextLine()) {
				String name = scanner.next();
				name = name.replaceAll("\"", "");
				names.add(name);
			}
		} catch  (FileNotFoundException e) {
			e.printStackTrace();
		}

		// Sort list of names
		Collections.sort(names);
		System.out.println(names);

		//Calculate score for each name
		Euler22 euler22 = new Euler22();
		int totalScore = 0;
		for(int i = 0; i < names.size(); i++) {
			int nameScore = euler22.calculateScore(names.get(i), i+1);
			totalScore += nameScore;
		}
		System.out.println(totalScore);
	}
}