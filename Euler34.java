public class Euler34{
	public static int factorial(int n){
		if(n == 0) return 1;
		else return n*factorial(n-1);		
	}

	public static boolean isCuriousNo(String number){
		int sum = 0;
		for(int i = 0; i < number.length(); i++){
			sum += factorial(number.charAt(i)-48);
		}
		
		if(sum == Integer.parseInt(number)) return true;
		else return false;
	}
	public static void main(String[] args){
		int curiousSum = 0;
		
		for(int i = 10; i <= 50000; i++){
			if(isCuriousNo(new String(""+i))) curiousSum += i;
		}
		
		System.out.println(curiousSum);
	}
}
