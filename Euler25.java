import java.math.*;

public class Euler25{
	public static int digits(BigInteger n){
		String number = n.toString();
		return(number.length());
	}

	public static void main(String[] args){
		int termCounter = 3;
		BigInteger start = new BigInteger(""+1);
		BigInteger current = new BigInteger(""+2);
		BigInteger temp = new BigInteger(""+0);
		
		while(digits(new BigInteger(""+current))!= 1000){
			temp = current.add(start);
			start = current;
			current = temp;
			termCounter++;
		}
		System.out.println(termCounter);
	}
}
