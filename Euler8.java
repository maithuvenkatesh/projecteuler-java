import java.io.*;
import java.util.*;

public class Euler8{
	
	public static void main(String[] args) throws FileNotFoundException{
		FileReader fr = new FileReader("Euler8.txt");
		Scanner scan = new Scanner(fr);	
		ArrayList<String> numbers = new ArrayList<String>();
		
		// Read in 1000 digit number - it's broken into lines
		while(scan.hasNext()){
			numbers.add((scan.next()));
		}
		
		// Convert to string - replace empty spaces with 1's
		String numString = "";
		for(String n:numbers){
			if(n == "") n = "1";
			numString += n;
		}
		
		int largestProduct = 0;
	
		for(int i = 0; i <= numString.length()-5; i++){
			int a,b,c,d,e,temp = 0;
			a = Integer.parseInt(Character.toString(numString.charAt(i)));
			b = Integer.parseInt(Character.toString(numString.charAt(i+1)));
			c = Integer.parseInt(Character.toString(numString.charAt(i+2)));
			d = Integer.parseInt(Character.toString(numString.charAt(i+3)));
			e = Integer.parseInt(Character.toString(numString.charAt(i+4)));
			temp = a * b * c * d * e;
			//System.out.println(largestProduct = temp);
			
			if(temp > largestProduct) {
				largestProduct = temp;
				System.out.println(largestProduct);
				//System.exit(0);
			}
		}
	}	
}

