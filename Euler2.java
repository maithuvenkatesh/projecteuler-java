public class Euler2{
	public static void main(String[] args){
		int start = 1;
		int current = 2;
		int temp = 0;
		int end = 4000000;
		int sum = current;
		
		while(end >= temp){
			temp = start + current;
			start = current;
			current = temp;
			
			if(temp % 2 == 0) sum += temp;
		}
		
		System.out.println(sum);
	}
}
