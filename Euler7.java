import java.util.*;

public class Euler7{
	
	public static void main(String[] args){
		ArrayList<Integer> primes = new ArrayList<Integer>();
		boolean[] list = PrimeSievePrimitive.primeList(200000);
		
		
		for(int i = 2; i < list.length; i++){
			if(list[i]) primes.add(new Integer(i));
		}
		
		Integer primeNo = new Integer(primes.get(10000)); //ArrayList primes is 0 based, hence 10000th prime is the 10001st prime!
		System.out.print(primeNo);
	
	}
}
