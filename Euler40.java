public class Euler40{
	public static void main(String[] args){
		StringBuffer digit = new StringBuffer();
		for(int i = 1; digit.length() < 1000000; i++){
			digit.append(String.valueOf(i));
		}

		int d1 = Integer.parseInt("" + digit.charAt(0));
		int d10 = Integer.parseInt("" + digit.charAt(9));
		int d100 = Integer.parseInt("" + digit.charAt(99));
		int d1000 = Integer.parseInt("" + digit.charAt(999));
		int d10000 = Integer.parseInt("" + digit.charAt(9999));
		int d100000 = Integer.parseInt("" + digit.charAt(99999));
		int d1000000 = Integer.parseInt("" + digit.charAt(999999));

		int answer = d1*d10*d100*d1000*d10000*d100000*d1000000;
		System.out.println(Integer.parseInt("" + digit.charAt(11)));
		System.out.println("The answer is: " + answer);

	}
}