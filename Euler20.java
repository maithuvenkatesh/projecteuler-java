import java.math.*;

public class Euler20{
	BigInteger factorial(long n){
		BigInteger result = new BigInteger(String.valueOf(n));
		
		if(n == 0) return BigInteger.ONE; //base case
		else return result.multiply(factorial(n-1));
	}
	
	public static void main(String[] args){
		Euler20 e20 = new Euler20();
		BigInteger facResult = e20.factorial(100);
		String facString = facResult.toString();
		ArrayList<Integer> factorialDigits = new ArrayList<Integer>();
		for(int i = 0; i < facString.length(); i++){
			String temp = facString.charAt(i);
			factorialDigits.add(temp);
		}
			
	}
}


