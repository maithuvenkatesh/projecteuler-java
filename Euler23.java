import java.util.*;

public class Euler23{
	
	public static boolean isAbundantNo(int n){	
		int sumDivisors = 0;
		for(int i = 1; i <= n/2; i++){
			if(n % i == 0) sumDivisors += i;
		}	
		return(sumDivisors > n);
	}

	public static void main(String[] args){
		//Calculate all abundant numbers from 12 to 28123
		ArrayList<Integer> abundantNumbers = new ArrayList<Integer>();
		for(int i = 12; i < 28123; i++){
			if(isAbundantNo(i)) abundantNumbers.add(i);
		} 
		
		//Calculate all the sums of abundant numbers
		HashMap abundantSums = new HashMap();
		for(int i = 0; i < abundantNumbers.size(); i++){
			for(int j = i; j < abundantNumbers.size(); j++){
				int temp = abundantNumbers.get(i) + abundantNumbers.get(j);
				if(temp < 28123){
					abundantSums.put(new Integer(temp), new Integer(temp));
				}
			}
		}
		
		int sum = 0;
		for(int i = 1; i < 28123; i++){
			if(!(abundantSums.containsKey(i))) sum += i;
		}
		System.out.println(sum);
	}
}
	
